<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CallApiController extends AbstractController
{
    #[Route('/call/api', name: 'call_api')]
    public function index(): Response
    {

            $date = new \DateTime;
    
            $getTimeStamp = $date->getTimestamp();  
            $startTime = $getTimeStamp - '86400';
            $dateNow = $date->getTimestamp();
    
            $responses = $this->client->request( 
                'GET', 
                'https://api.binance.com/api/v3/klines?symbol=BTCEUR&interval=1h&startTime=' . $startTime . '000&endTime=' . $dateNow . '000&limit=500',
                [ 'headers' => [ 
                    'X-MBX-APIKEY' => 'NKw5LsZqXhjD9w7EhUik7Wg98sIpmVwdUm6QFV5Hl6vKwORkdQdyZ7BlvHMrbIV5'
                ],
            ]);
    
            $content = $responses->getContent();
            $content = $responses->toArray();
            
            
            return $content;
        
    }
}
