<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AvgPriceController extends AbstractController
{
    #[Route('/avg/price', name: 'avg_price')]
    public function index(HttpClientInterface $client): Response
    {
        $oneHour = '3600';
        $oneDay = '86400';
        $oneWeek = '604800';
        $oneMonth = '2592000';
        $threeMonth = '7776000';
        $sixMonth = '15552000';
        $oneYear = '31536000';

        // timestamp du début 
        $date = new \DateTime();
        $dateNow = $date->getTimestamp();

        $timeStampStartTime = $date->getTimestamp();


        if (isset($_POST['selectDate'])) {
            $selectDate = isset($_POST['$selectDate']);
            switch ($selectDate) {
                case 'oneHour':
                    $timeStampStartTime = $dateNow - $oneHour;
                    break;
                case 'oneDay':
                    $timeStampStartTime = $dateNow - $oneDay;
                    break;
                case 'oneWeek':
                    $timeStampStartTime = $dateNow - $oneWeek;
                    break;
                case 'oneMonth':
                    $timeStampStartTime = $dateNow - $oneMonth;
                    break;
                case 'threeMonth':
                    $timeStampStartTime = $dateNow - $threeMonth;
                    break;
                case 'sixMonth':
                    $timeStampStartTime = $dateNow - $sixMonth;
                    break;
                case 'oneYear':
                    $timeStampStartTime = $dateNow - $oneYear;
                    break;
                default:
                    echo 'Veuillez selectionner une date';
                    break; 
            }
        }
    }
}