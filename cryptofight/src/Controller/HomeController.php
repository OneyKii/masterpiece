<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AvgPrice 
{
    
}

class HomeController extends AbstractController
{
        // TODO : récupérer la valeur du select
        // le transformer en timestamp
        // timestamp - varibale prédéfinie

    #[Route('/', name: 'home')]
    public function index(HttpClientInterface $client): Response 
    {

        $date = new \DateTime();
        $dateNow = $date->getTimestamp();

        /* $oneHour = '3600';
        $oneDay = '86400';
        $oneWeek = '604800';
        $oneMonth = '2592000';
        $threeMonth = '7776000';
        $sixMonth = '15552000';
        $oneYear = '31536000'; */

        if (isset($_POST['selectDate'])) {
            $selectDate = $_POST['selectDate'];
        } else if (isset($_GET['selectDate'])) {
            $selectDate = $_GET['selectDate'];
        } else {
            $selectDate = 'choose';
        }


            $jsonString = new CallApiService($client);

            $result = $jsonString->avgPrice();
            $result2 = $jsonString->avgPrice();

            $getUrl =  AvgPrice();

        return $this->render('home/index.html.twig', [
            // player 1 & 2
           'PlayerOne' => $result,
           'PlayerTwo' => $result2,
            // timestamp now
           'date' => $dateNow,
           // print date selected 
           // 'values' => $values,
           'getUrl' => $getUrl,
            ]
        );
    }

}
