<?php
 
namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
 
class CallApiService
{
    private $client;
 
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }


    public function avgPrice(): array 
    {

        $oneHour = '3600';
        $oneDay = '86400';
        $oneWeek = '604800';
        $oneMonth = '2592000';
        $threeMonth = '7776000';
        $sixMonth = '15552000';
        $oneYear = '31536000';

        // timestamp du début 
        $date = new \DateTime();
        $dateNow = $date->getTimestamp();

        $timeStampStartTime = $date->getTimestamp();


        if (isset($_POST['selectDate'])) {
            $selectDate = isset($_POST['$selectDate']);
            switch ($selectDate) {
                case 'oneHour':
                    $timeStampStartTime = $dateNow - $oneHour;
                    break;
                case 'oneDay':
                    $timeStampStartTime = $dateNow - $oneDay;
                    break;
                case 'oneWeek':
                    $timeStampStartTime = $dateNow - $oneWeek;
                    break;
                case 'oneMonth':
                    $timeStampStartTime = $dateNow - $oneMonth;
                    break;
                case 'threeMonth':
                    $timeStampStartTime = $dateNow - $threeMonth;
                    break;
                case 'sixMonth':
                    $timeStampStartTime = $dateNow - $sixMonth;
                    break;
                case 'oneYear':
                    $timeStampStartTime = $dateNow - $oneYear;
                    break;
                default:
                    echo 'Veuillez selectionner une date';
                    break; 
            }
        }

        

        // Date de début
        $startTime = $timeStampStartTime - '86400';

        $responses = $this->client->request( 
            'GET', 
            'https://api.binance.com/api/v3/klines?symbol=BTCEUR&interval=1h&startTime=' . $startTime . '000&endTime=' . $dateNow . '000&limit=500',
            [ 'headers' => [ 
                'X-MBX-APIKEY' => 'NKw5LsZqXhjD9w7EhUik7Wg98sIpmVwdUm6QFV5Hl6vKwORkdQdyZ7BlvHMrbIV5'
            ],
        ]);

        $content = $responses->getContent();
        $content = $responses->toArray();
        
        
        return $content;
    }
}
